package org.myproject;

public class Triangle {

    public Point[] points;

    public Triangle(Point a, Point b, Point c) {
        this.points = new Point[] {a, b, c};
    }

    public boolean isCCW() {
        return Utils.ccwOrientation(this.points[0], this.points[1], this.points[2]);
    }

    public int hasCommonEdge(Triangle triangle) {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                if (this.points[i] == triangle.points[(j + 1)%3] && this.points[(i + 1)%3] == triangle.points[j])
                    return 10*((i + 2)%3) + ((j + 2)%3);
        return -1;
    }

    public String toString() {
        return String.format("(%s %s %s)", this.points[0].name, this.points[1].name, this.points[2].name);
    }
    
}
