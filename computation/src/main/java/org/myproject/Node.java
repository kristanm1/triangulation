package org.myproject;

import java.util.LinkedList;
import java.util.List;

public class Node {
    
    public Triangle triangle;
    public Node parent1;
    public Node parent2;
    public Node[] children;

    public Node(Triangle triangle) {
        this.triangle = triangle;
        this.parent1 = null;
        this.parent2 = null;
        this.children = null;
    }

    public Node(Triangle triangle, Node parent1) {
        this.triangle = triangle;
        this.parent1 = parent1;
        this.parent2 = null;
        this.children = null;
    }

    public Node(Triangle triangle, Node parent1, Node parent2) {
        this.triangle = triangle;
        this.parent1 = parent1;
        this.parent2 = parent2;
        this.children = null;
    }

    public Node(Triangle triangle, Node parent1, Node parent2, Node[] children) {
        this.triangle = triangle;
        this.parent1 = parent1;
        this.parent2 = parent2;
        this.children = children;
    }

    public List<Node> getGrandparents() {
        List<Node> grandparents = new LinkedList<>();
        if (this.parent1 != null) {
            if (this.parent1.parent1 != null) {
                grandparents.add(this.parent1.parent1);
            }
            if (this.parent1.parent2 != null) {
                grandparents.add(this.parent1.parent2);
            }
        }
        if (this.parent2 != null) {
            if (this.parent2.parent1 != null) {
                grandparents.add(this.parent2.parent1);
            }
            if (this.parent2.parent2 != null) {
                grandparents.add(this.parent2.parent2);
            }
        }
        return grandparents;
    }

    public String toString() {
        return String.format(
            "#parents %d #children %d %s",
            parent1 == null ? 0 : this.parent2 == null ? 1 : 2,
            this.children != null ? this.children.length : 0,
            this.triangle.toString()
        );
    }

}
