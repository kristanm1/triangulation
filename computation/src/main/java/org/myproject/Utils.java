package org.myproject;

public class Utils {

    public static final double EPS = 1e-12;

    public static double det2x2(
        double a, double b,
        double c, double d
    ) {
        return a*d - b*c;
    }

    public static double det3x3(double a, double b, double c, double d, double e, double f, double g, double h, double i) {
            return c*det2x2(d, e, g, h) - f*det2x2(a, b, g, h) + i*det2x2(a, b, d, e);
    }

    public static boolean ccwOrientation(Point a, Point b, Point c) {
        double px = b.x - a.x;
        double py = b.y - a.y;
        double qx = c.x - a.x;
        double qy = c.y - a.y;
        return det2x2(px, py, qx, qy) > EPS;
    }
    
    public static boolean insideCircumcircle(Point a, Point b, Point c, Point d) {
        double px = a.x - d.x;
        double py = a.y - d.y;
        double qx = b.x - d.x;
        double qy = b.y - d.y;
        double rx = c.x - d.x;
        double ry = c.y - d.y;
        double dxSq = d.x*d.x;
        double dySq = d.y*d.y;
        double det = det3x3(
            px, py, (a.x*a.x - dxSq) + (a.y*a.y - dySq),
            qx, qy, (b.x*b.x - dxSq) + (b.y*b.y - dySq),
            rx, ry, (c.x*c.x - dxSq) + (c.y*c.y - dySq)
        );
        //System.out.println(det);
        return det > EPS;
    }

    public static boolean insideTriangle(Triangle triangle, Point point) {
        Point a = triangle.points[0];
        Point b = triangle.points[1];
        Point c = triangle.points[2];
        double det = ((a.x - c.x)*(b.y - c.y) - (b.x - c.x)*(a.y - c.y));
        if (det < EPS && det > -EPS)
            return false;
        double ra = ((b.y - c.y)*(point.x - c.x) + (c.x - b.x)*(point.y - c.y))/det;
        double rb = ((c.y - a.y)*(point.x - c.x) + (a.x - c.x)*(point.y - c.y))/det;
        return ra + rb <= 1.0 && ra >= 0.0 && rb >= 0.0;
    }

}
