package org.myproject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.myproject.Utils.insideTriangle;

public class IncrementalTriangulation {
    
    private List<Point> points;
    private List<Point> auxiliaryPoints;
    private Point min;
    private Point max;
    private Node historyGraph;

    public IncrementalTriangulation(List<Point> points) {
        this.points = new ArrayList<>(points.size());
        this.auxiliaryPoints = new ArrayList<>(3);
        this.min = new Point(Double.MAX_VALUE, Double.MAX_VALUE);
        this.max = new Point(Double.MIN_VALUE, Double.MIN_VALUE);
        for (Point point : points) {
            if (point.x < this.min.x)
                this.min.x = point.x;
            if (this.max.x < point.x)
                this.max.x = point.x;
            if (point.y < this.min.y)
                this.min.y = point.y;
            if (this.max.y < point.y)
                this.max.y = point.y;
            System.out.println(point.name + " " + point.toString());
            this.points.add(point);
        }
    }

    public Triangle getAuxiliaryTriangle() {
        double dx = this.max.x - this.min.x;
        double dy = this.max.y - this.min.y;
        Point a = new Point(this.min.x - 2.0*dx, max.y + 2.0*dy);
        Point b = new Point((this.min.x + this.max.x)/2.0, this.min.y - 2.0*dy);
        Point c = new Point(this.max.x + 2.0*dx, this.max.y + 2.0*dy);
        
        a = new Point(-10.0, 10.0, "x");
        b = new Point(3.0, -5.0, "y");
        c = new Point(15.0, 10.0, "z");

        this.auxiliaryPoints.add(a);
        this.auxiliaryPoints.add(b);
        this.auxiliaryPoints.add(c);
        System.out.println("Auxiliar points");
        System.out.println(a.name + " " + a.toString());
        System.out.println(b.name + " " + b.toString());
        System.out.println(c.name + " " + c.toString());
        System.out.println();
        return new Triangle(a, b, c);
    }

    public Node findAndReturnContainingNode(Node node, Point point) {
        if (node.children == null)
            return node;
        else {
            for (Node child : node.children) {
                if (insideTriangle(child.triangle, point)) {
                    return findAndReturnContainingNode(child, point);
                }
            }
        }
        return null;
    }


    public void findAndFlip(Node current, Node node) {
        int opposite = current.triangle.hasCommonEdge(node.triangle);
        List<Node> tempGranNodes = current.getGrandparents();
        System.out.printf("%2d %s(%d) %s", opposite, current.triangle.toString(), tempGranNodes.size(), node.triangle.toString());
        if (opposite != -1) {
            System.out.printf(" -> %b (%s %s %s, %s)\n", 
                Utils.insideCircumcircle(
                    current.triangle.points[0],
                    current.triangle.points[1],
                    current.triangle.points[2],
                    node.triangle.points[opposite%10]
                ),
                current.triangle.points[0].name,
                current.triangle.points[1].name,
                current.triangle.points[2].name,
                node.triangle.points[opposite%10].name
            );
        } else {
            System.out.println();
        }
        if (opposite != -1) {

            if (node.children == null) {

                int oppositeCurrent = opposite/10;
                int oppositeNode = opposite%10;

                if (Utils.insideCircumcircle(
                    current.triangle.points[0],
                    current.triangle.points[1],
                    current.triangle.points[2],
                    node.triangle.points[oppositeNode]
                )) {

                    Node[] children = new Node[] {
                        new Node(
                            new Triangle(
                                current.triangle.points[oppositeCurrent],
                                node.triangle.points[oppositeNode],
                                node.triangle.points[(oppositeNode + 1)%3]
                            ),
                            current, node
                        ),
                        new Node(
                            new Triangle(
                                node.triangle.points[oppositeNode],
                                current.triangle.points[oppositeCurrent],
                                current.triangle.points[(oppositeCurrent + 1)%3]
                            ),
                            current, node
                        )
                    };
                    current.children = children;
                    node.children = children;

                    System.out.printf("Current triangle %s\n", current.triangle.toString());
                    System.out.printf("   Node triangle %s\n", node.triangle.toString());
                    System.out.printf("-> %s\n", children[0].triangle.toString());
                    System.out.printf("-> %s\n", children[1].triangle.toString());
    
                    for (Node child : children) {
                        List<Node> grandparents = child.getGrandparents();
    
                        for (Node grandparent : grandparents) {
                            for (Node parentSybling : grandparent.children) {
                                if (!(
                                    child.parent1 == parentSybling ||
                                    (child.parent2 != null && child.parent2 == parentSybling) ||
                                    (grandparents.contains(parentSybling))
                                )) {
                                    findAndFlip(child, parentSybling);
                                }
                            }
                        }
                    }

                }
                

            } else {
                for (Node child : node.children) {
                    findAndFlip(current, child);
                }
            }

        }
    }

    public void getAllTriangles(Node node, Set<Triangle> triangles) {
        if (node.children == null) {
            if (!(
                this.auxiliaryPoints.contains(node.triangle.points[0]) ||
                this.auxiliaryPoints.contains(node.triangle.points[1]) ||
                this.auxiliaryPoints.contains(node.triangle.points[2])
            )) {
                triangles.add(node.triangle);
            } else {
                triangles.add(node.triangle);
            }
        }
        else {
            for (Node child : node.children) {
                getAllTriangles(child, triangles);
            }
        }
    }

    public void triangulate() {

        //Collections.shuffle(this.points);

        this.historyGraph = new Node(this.getAuxiliaryTriangle());

        for (Point point : this.points) {

            System.out.printf("Inserting point %s\n", point.name);
            //System.out.println("History graph");
            //displayHistoryGraph(this.historyGraph, 0);

            Node newParent = this.findAndReturnContainingNode(historyGraph, point);
            newParent.children = new Node[] {
                new Node(new Triangle(newParent.triangle.points[0], newParent.triangle.points[1], point), newParent),
                new Node(new Triangle(newParent.triangle.points[1], newParent.triangle.points[2], point), newParent),
                new Node(new Triangle(newParent.triangle.points[2], newParent.triangle.points[0], point), newParent)
            };

            System.out.printf("Containing triangle %s\n", newParent.triangle.toString());
            System.out.printf("-> %s\n", newParent.children[0].triangle.toString());
            System.out.printf("-> %s\n", newParent.children[1].triangle.toString());
            System.out.printf("-> %s\n", newParent.children[2].triangle.toString());

            for (Node child : newParent.children) {
                
                List<Node> grandparents = child.getGrandparents();
                for (Node grandparent : grandparents) {
                    for (Node parentSybling : grandparent.children)  {
                        if (newParent != parentSybling) {
                            findAndFlip(child, parentSybling);
                        }
                    }
                    //System.out.println();
                }
                        
                
            }

            System.out.println();

        }

        //displayHistoryGraph(this.historyGraph, 0);

        Set<Triangle> triangles = new HashSet<>();
        this.getAllTriangles(this.historyGraph, triangles);


        Iterator<Triangle> triangleIterator = triangles.iterator();
        String[] lines = new String[triangles.size()];
        for (int i = 0; i < lines.length; i++) {
            Triangle triangle = triangleIterator.next();
            lines[i] = String.format(
                "\t[%f, %f, %f, %f, %f, %f]",
                triangle.points[0].x, triangle.points[0].y,
                triangle.points[1].x, triangle.points[1].y,
                triangle.points[2].x, triangle.points[2].y
            );
        }

        //System.out.printf("Number of triangles %d\n", triangles.size());
        System.out.println("T = [");
        System.out.println(String.join(",\n", lines));
        System.out.println("];");

    }

    public static void displayHistoryGraph(Node node, int depth) {
        for (int i = 0; i < depth; i++) {
            System.out.print("\t");
        }
        System.out.printf("%s\n", node.toString());
        if (node.children != null) {
            for (Node child : node.children) {
                displayHistoryGraph(child, depth + 1);
            }
        }
    }


}
