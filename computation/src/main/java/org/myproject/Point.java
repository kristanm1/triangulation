package org.myproject;

public class Point {
    
    public double x;
    public double y;
    public String name;

    public Point() {}

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(double x, double y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    public String toString() {
        return String.format("(%f, %f)", this.x, this.y);
    }

}
