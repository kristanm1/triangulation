package org.myproject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.myproject.Utils.insideTriangle;

public class IncrementalTriangulationTest {
    
    @Test
    public void auxiliaryTriangleTest() {
        int n = 1000;
        List<Point> points = new ArrayList<>(n);
        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            Point point = new Point(rand.nextDouble(), rand.nextDouble());
            points.add(point);
        }
        IncrementalTriangulation it = new IncrementalTriangulation(points);
        Triangle triangle = it.getAuxiliaryTriangle();
        for (Point point : points) {
            assertTrue(insideTriangle(triangle, point));
        }
    }

    @Test
    public void incrementalTriangulationTest() {

        List<Point> points = Arrays.asList(new Point[] {
            new Point(5.0, 4.0, "a"),
            new Point(3.0, 6.0, "b"),
            new Point(0.5, 0.0, "c"),
            new Point(5.0, 5.0, "d"),
            new Point(6.0, 1.0, "e"),
            new Point(6.0, 8.0, "f")
        });
        
        /*
        Random rand = new Random();
        int n = 6;
        points = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            points.add(new Point(rand.nextDouble(), rand.nextDouble()));
        }
        */
        

        IncrementalTriangulation triangulation = new IncrementalTriangulation(points);
        triangulation.triangulate();

    }

}
