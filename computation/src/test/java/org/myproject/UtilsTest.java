package org.myproject;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.myproject.Utils.det3x3;
import static org.myproject.Utils.ccwOrientation;
import static org.myproject.Utils.insideCircumcircle;
import static org.myproject.Utils.insideTriangle;

import org.junit.Test;

public class UtilsTest {

    @Test
    public void det3x3Test() {
        double det = det3x3(
            2.5, -5, 31,
            4.5, 4, 36,
            2.5, 6, 42
        );
        System.out.println(det);
    }
    
    @Test
    public void ccwPointsTest() {
        Point a = new Point(1.0, 0.0);
        Point b = new Point(1.0, 1.0);
        Point c = new Point(0.0, 1.0);
        assertTrue(ccwOrientation(a, b, c));
    }

    @Test
    public void cwPointsTest() {
        Point a = new Point(1.0, 0.0);
        Point b = new Point(0.0, 1.0);
        Point c = new Point(1.0, 1.0);
        assertFalse(ccwOrientation(a, b, c));
    }

    @Test
    public void colinearPointsTest() {
        Point a = new Point(0.0, 0.0);
        Point b = new Point(1.0, 1.0);
        Point c = new Point(2.0, 2.0);
        assertFalse(ccwOrientation(a, b, c));
    }

    @Test
    public void insideCircumcircleTest() {
        Point a = new Point(0.0, 0.0);
        Point b = new Point(0.0, 1.0);
        Point c = new Point(1.0, 1.0);
        Point d = new Point(1.0, 0.9);
        assertTrue(insideCircumcircle(a, b, c, d));
    }

    @Test
    public void outsideCircumcircleTest() {
        Point a = new Point(0.0, 0.0);
        Point b = new Point(1.0, 0.0);
        Point c = new Point(1.0, 1.0);
        Point d = new Point(1.0, 1.1);
        assertFalse(insideCircumcircle(a, b, c, d));
    }

    @Test
    public void onCircumcircleTest() {
        Point a = new Point(0.0, 0.0);
        Point b = new Point(1.0, 0.0);
        Point c = new Point(1.0, 1.0);
        Point d = new Point(1.0, 1.0);
        assertFalse(insideCircumcircle(a, b, c, d));
    }

    @Test
    public void test() {
        Point a = new Point(5.0, 4.0, "a");
        Point b = new Point(3.0, 6.0, "b");
        Point c = new Point(0.5, 0.0, "c");
        Point d = new Point(5.0, 5.0, "d");
        Point e = new Point(6.0, 1.0, "e");
        Point f = new Point(6.0, 7.0, "f");

        Point x = new Point(-10.0, 10.0, "x");
        Point y = new Point(3.0, -5.0, "y");
        Point z = new Point(15.0, 10.0, "z");

        Point p1 = b;
        Point p2 = a;
        Point p3 = d;
        Point p4 = c;

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println("ccw:" + Utils.ccwOrientation(p1, p2, p3));
        System.out.println(p4);

        System.out.println("inside: " + insideCircumcircle(p1, p2, p3, p4));
    }

    @Test
    public void insideTriangleTest() {
        Triangle triangle = new Triangle(
            new Point(0.0, 0.0),
            new Point(1.0, 0.0),
            new Point(0.0, 1.0)
        );
        Point point = new Point(0.5, 0.25);
        assertTrue(insideTriangle(triangle, point));
    }
    
    @Test
    public void outsideTriangleTest() {
        Triangle triangle = new Triangle(
            new Point(0.0, 0.0),
            new Point(1.0, 0.0),
            new Point(0.0, 1.0)
        );
        Point point = new Point(1.0, 1.0);
        assertFalse(insideTriangle(triangle, point));
    }

}
