package org.myproject;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TriangleTest {

    @Test
    public void hasCommonEdgeTest() {
        Point a = new Point(0.0, 0.0);
        Point b = new Point(1.0, 0.0);
        Point c = new Point(1.0, 1.0);
        Point d = new Point(0.0, 1.0);
        Triangle t1 = new Triangle(a, b, c);
        Triangle t2 = new Triangle(c, d, a);
        assertEquals(11, t1.hasCommonEdge(t2));
    }
    
}
